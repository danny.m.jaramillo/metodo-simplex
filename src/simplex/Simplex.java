/*
 * MetodoSimplexApp.java
 */

package simplex;

import org.jdesktop.application.Application;
import org.jdesktop.application.SingleFrameApplication;

public class Simplex extends SingleFrameApplication {

   
    @Override protected void startup() {
        show(new SimplexView(this));
    }

 
    @Override protected void configureWindow(java.awt.Window root) {
    }

//    public static MetodoSimplexApp getApplication() {
//        return Application.getInstance(MetodoSimplexApp.class);
//    }

//  Metodo Principal
    
    public static void main(String[] args) {
        launch(Simplex.class, args);
    }
}
