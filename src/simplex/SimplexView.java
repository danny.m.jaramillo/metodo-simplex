/*
 * MetodoSimplexView.java
 */
package simplex;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Enumeration;
import java.util.concurrent.TimeUnit;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableColumnModelListener;
import javax.swing.table.TableColumn;
import simplex.formulas.ClaseSimplex;
import org.jdesktop.application.Action;
import org.jdesktop.application.ResourceMap;
import org.jdesktop.application.SingleFrameApplication;
import org.jdesktop.application.FrameView;
import org.jdesktop.application.TaskMonitor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.BitSet;
import java.util.concurrent.Delayed;
import javax.swing.Timer;
import javax.swing.Icon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import org.w3c.dom.css.RGBColor;

public class SimplexView extends FrameView {

    public SimplexView(SingleFrameApplication app) {
        super(app);

        
        initComponents();
        //Boton Nueva Funcion
        btnNewFunction.setVisible(false);

        ResourceMap resourceMap = getResourceMap();
        int messageTimeout = resourceMap.getInteger("StatusBar.messageTimeout");
        messageTimer = new Timer(messageTimeout, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
//                statusMessageLabel.setText("");
            }
        });
        messageTimer.setRepeats(false);
        int busyAnimationRate = resourceMap.getInteger("StatusBar.busyAnimationRate");
        for (int i = 0; i < busyIcons.length; i++) {
//            busyIcons[i] = resourceMap.getIcon("StatusBar.busyIcons[" + i + "]");
        }
        busyIconTimer = new Timer(busyAnimationRate, new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                busyIconIndex = (busyIconIndex + 1) % busyIcons.length;
            }
        });
        idleIcon = resourceMap.getIcon("StatusBar.idleIcon");

        // connecting action tasks to status bar via TaskMonitor
        TaskMonitor taskMonitor = new TaskMonitor(getApplication().getContext());
        taskMonitor.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                String propertyName = evt.getPropertyName();
                if ("started".equals(propertyName)) {
                    if (!busyIconTimer.isRunning()) {
                        busyIconIndex = 0;
                        busyIconTimer.start();
                    }
                } else if ("done".equals(propertyName)) {
                    busyIconTimer.stop();
                } else if ("message".equals(propertyName)) {
                    String text = (String) (evt.getNewValue());
                    messageTimer.restart();
                } else if ("progress".equals(propertyName)) {
                    int value = (Integer) (evt.getNewValue());
                }
            }
        });
    }


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        mainPanel = new javax.swing.JPanel();
        btnresolver = new java.awt.Button();
        areaFObj = new javax.swing.JPanel();
        txtfobjetivo = new java.awt.TextField();
        cmbmaxmin = new javax.swing.JComboBox();
        btnNewFunction = new javax.swing.JButton();
        areaRest = new javax.swing.JPanel();
        cmbsimbolo = new javax.swing.JComboBox();
        txtnsubindices = new java.awt.TextField();
        btnlimpiar = new java.awt.Button();
        btnagregarRestriccion = new java.awt.Button();
        txtnvalor = new java.awt.TextField();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaRestricciones = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        funcionEvaluar = new javax.swing.JTextArea();
        listaResultado = new java.awt.List();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        funcionIteracciones = new javax.swing.JTextArea();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnNewWindow = new javax.swing.JButton();

        mainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        mainPanel.setMaximumSize(new java.awt.Dimension(700, 400));
        mainPanel.setMinimumSize(new java.awt.Dimension(600, 300));
        mainPanel.setName("mainPanel"); // NOI18N
        mainPanel.setPreferredSize(new java.awt.Dimension(800, 900));
        mainPanel.setRequestFocusEnabled(false);

        org.jdesktop.application.ResourceMap resourceMap = org.jdesktop.application.Application.getInstance(simplex.Simplex.class).getContext().getResourceMap(SimplexView.class);
        btnresolver.setLabel(resourceMap.getString("btnresolver.label")); // NOI18N
        btnresolver.setName("btnresolver"); // NOI18N
        btnresolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnresolverActionPerformed(evt);
            }
        });

        areaFObj.setBackground(resourceMap.getColor("areaFObj.background")); // NOI18N
        areaFObj.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        areaFObj.setToolTipText(resourceMap.getString("areaFObj.toolTipText")); // NOI18N
        areaFObj.setName("areaFObj"); // NOI18N
        areaFObj.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                areaFObjMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                areaFObjMouseExited(evt);
            }
        });

        txtfobjetivo.setName("txtfobjetivo"); // NOI18N
        txtfobjetivo.setText(resourceMap.getString("txtfobjetivo.text")); // NOI18N

        cmbmaxmin.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Función Objetivo (Z) =" }));
        cmbmaxmin.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cmbmaxmin.setKeySelectionManager(null);
        cmbmaxmin.setName("cmbmaxmin"); // NOI18N
        cmbmaxmin.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbmaxminItemStateChanged(evt);
            }
        });

        btnNewFunction.setText(resourceMap.getString("btnNewFunction.text")); // NOI18N
        btnNewFunction.setName("btnNewFunction"); // NOI18N
        btnNewFunction.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewFunctionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout areaFObjLayout = new javax.swing.GroupLayout(areaFObj);
        areaFObj.setLayout(areaFObjLayout);
        areaFObjLayout.setHorizontalGroup(
            areaFObjLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(areaFObjLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(cmbmaxmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtfobjetivo, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(btnNewFunction, javax.swing.GroupLayout.PREFERRED_SIZE, 156, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        areaFObjLayout.setVerticalGroup(
            areaFObjLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(areaFObjLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(areaFObjLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtfobjetivo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbmaxmin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNewFunction))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        areaRest.setBackground(resourceMap.getColor("areaRest.background")); // NOI18N
        areaRest.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        areaRest.setName("areaRest"); // NOI18N

        cmbsimbolo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "<=" }));
        cmbsimbolo.setName("cmbsimbolo"); // NOI18N

        txtnsubindices.setName("txtnsubindices"); // NOI18N
        txtnsubindices.setText(resourceMap.getString("txtnsubindices.text")); // NOI18N

        btnlimpiar.setLabel(resourceMap.getString("btnlimpiar.label")); // NOI18N
        btnlimpiar.setName("btnlimpiar"); // NOI18N
        btnlimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlimpiarActionPerformed(evt);
            }
        });

        btnagregarRestriccion.setLabel(resourceMap.getString("btnagregarRestriccion.label")); // NOI18N
        btnagregarRestriccion.setName("btnagregarRestriccion"); // NOI18N
        btnagregarRestriccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnagregarRestriccionActionPerformed(evt);
            }
        });

        txtnvalor.setName("txtnvalor"); // NOI18N

        jLabel2.setText(resourceMap.getString("jLabel2.text")); // NOI18N
        jLabel2.setName("jLabel2"); // NOI18N

        javax.swing.GroupLayout areaRestLayout = new javax.swing.GroupLayout(areaRest);
        areaRest.setLayout(areaRestLayout);
        areaRestLayout.setHorizontalGroup(
            areaRestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(areaRestLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(areaRestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnlimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, 756, Short.MAX_VALUE)
                    .addComponent(btnagregarRestriccion, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 756, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(areaRestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtnsubindices, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(cmbsimbolo, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtnvalor, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
            .addGroup(areaRestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 277, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(489, Short.MAX_VALUE))
        );
        areaRestLayout.setVerticalGroup(
            areaRestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, areaRestLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(areaRestLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtnsubindices, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbsimbolo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtnvalor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnagregarRestriccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(btnlimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        tablaRestricciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", "", ""},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Valores Subindices", "Condicion", "Valor Máximo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        tablaRestricciones.setFocusable(false);
        tablaRestricciones.setName("tablaRestricciones"); // NOI18N
        jScrollPane1.setViewportView(tablaRestricciones);
        tablaRestricciones.getColumnModel().getColumn(0).setHeaderValue(resourceMap.getString("tablaRestricciones.columnModel.title0")); // NOI18N
        tablaRestricciones.getColumnModel().getColumn(1).setHeaderValue(resourceMap.getString("tablaRestricciones.columnModel.title1")); // NOI18N
        tablaRestricciones.getColumnModel().getColumn(2).setHeaderValue(resourceMap.getString("tablaRestricciones.columnModel.title2")); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel1.setName("jPanel1"); // NOI18N

        jLabel4.setText(resourceMap.getString("jLabel4.text")); // NOI18N
        jLabel4.setName("jLabel4"); // NOI18N

        jScrollPane3.setName("jScrollPane3"); // NOI18N

        funcionEvaluar.setColumns(20);
        funcionEvaluar.setRows(5);
        funcionEvaluar.setName("funcionEvaluar"); // NOI18N
        jScrollPane3.setViewportView(funcionEvaluar);

        listaResultado.setName("listaResultado"); // NOI18N

        jLabel1.setText(resourceMap.getString("jLabel1.text")); // NOI18N
        jLabel1.setName("jLabel1"); // NOI18N

        jLabel3.setText(resourceMap.getString("jLabel3.text")); // NOI18N
        jLabel3.setName("jLabel3"); // NOI18N

        jScrollPane2.setName("jScrollPane2"); // NOI18N

        funcionIteracciones.setColumns(20);
        funcionIteracciones.setRows(5);
        funcionIteracciones.setName("funcionIteracciones"); // NOI18N
        jScrollPane2.setViewportView(funcionIteracciones);

        jLabel5.setText(resourceMap.getString("jLabel5.text")); // NOI18N
        jLabel5.setName("jLabel5"); // NOI18N

        jLabel6.setText(resourceMap.getString("jLabel6.text")); // NOI18N
        jLabel6.setName("jLabel6"); // NOI18N

        jLabel7.setText(resourceMap.getString("jLabel7.text")); // NOI18N
        jLabel7.setName("jLabel7"); // NOI18N

        jLabel8.setText(resourceMap.getString("jLabel8.text")); // NOI18N
        jLabel8.setName("jLabel8"); // NOI18N

        jLabel9.setText(resourceMap.getString("jLabel9.text")); // NOI18N
        jLabel9.setName("jLabel9"); // NOI18N

        jLabel10.setText(resourceMap.getString("jLabel10.text")); // NOI18N
        jLabel10.setName("jLabel10"); // NOI18N

        jLabel11.setText(resourceMap.getString("jLabel11.text")); // NOI18N
        jLabel11.setName("jLabel11"); // NOI18N

        btnNewWindow.setText(resourceMap.getString("btnNewWindow.text")); // NOI18N
        btnNewWindow.setName("btnNewWindow"); // NOI18N
        btnNewWindow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNewWindowActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(58, 58, 58)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnNewWindow, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                            .addComponent(listaResultado, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(43, 43, 43)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 545, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addGap(29, 29, 29)
                        .addComponent(jLabel10)
                        .addGap(205, 205, 205))
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jLabel9)
                            .addComponent(jLabel5)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6)
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(listaResultado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNewWindow, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 782, Short.MAX_VALUE)
                            .addComponent(areaRest, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(areaFObj, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mainPanelLayout.createSequentialGroup()
                        .addGap(187, 187, 187)
                        .addComponent(btnresolver, javax.swing.GroupLayout.PREFERRED_SIZE, 416, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(areaFObj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(areaRest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(btnresolver, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        setComponent(mainPanel);
    }// </editor-fold>//GEN-END:initComponents
    //procedimiento proc= new procedimiento();
    private Problema problema;
    
    Tabla tablaImprimir;
    
    
    private int nRest = 0;    //numero de Restricciones
    private boolean accionMax = true; //maximizar=true , minimizar =false
    private void btnagregarRestriccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnagregarRestriccionActionPerformed
        tablaRestricciones.setValueAt(txtnsubindices.getText(), nRest, 0);
        tablaRestricciones.setValueAt(cmbsimbolo.getSelectedItem().toString(), nRest, 1);
        tablaRestricciones.setValueAt(txtnvalor.getText(), nRest, 2);
        nRest += 1;
        txtnsubindices.setText("");
        txtnvalor.setText("");
    }//GEN-LAST:event_btnagregarRestriccionActionPerformed
    private void btnlimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlimpiarActionPerformed
        limpiarContenido();
        problema.borrarTodo();
    }//GEN-LAST:event_btnlimpiarActionPerformed
    private void limpiarContenido(){
        listaResultado.clear();
        txtfobjetivo.setText(null);
        for (int i = 0; i < nRest; i++) {
            tablaRestricciones.setValueAt(null, i, 0);
            tablaRestricciones.setValueAt(null, i, 1);
            tablaRestricciones.setValueAt(null, i, 2);
        }
        nRest = 0;
    }
    
    private void desactivarCampos(){
        funcionEvaluar.setEditable(false);
        funcionIteracciones.setEditable(false);
        txtfobjetivo.setEditable(false);
        btnagregarRestriccion.setEnabled(false);
        btnNewFunction.setVisible(true);
    }
    
    private void activarCampos(){

        txtfobjetivo.setEditable(true);
        btnagregarRestriccion.setEnabled(true);
        btnNewFunction.setVisible(false);
    }
    
    String tabla1 = "";
    String intera = "";     
    
    private void btnresolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnresolverActionPerformed
        problema = new Problema(accionMax);
        listaResultado.clear();
        problema.borrarTodo();
        problema.setFuncionObjetivo(ClaseSimplex.capturar(txtfobjetivo.getText()));
    
        for (int i = 0; i < nRest; i++) {
            problema.nuevaRestriccion(tablaRestricciones.getValueAt(i, 0).toString(), tablaRestricciones.getValueAt(i, 1).toString(), tablaRestricciones.getValueAt(i, 2).toString());
        }
        
        tabla1 = problema.preparar();
        funcionEvaluar.setText(tabla1);
        
        intera=problema.resolverMetodoSimplex(listaResultado);
        funcionIteracciones.setText(intera);
        
        desactivarCampos();
        
    }//GEN-LAST:event_btnresolverActionPerformed
    private void cmbmaxminItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbmaxminItemStateChanged

    }//GEN-LAST:event_cmbmaxminItemStateChanged
    private void areaFObjMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areaFObjMouseEntered
    }//GEN-LAST:event_areaFObjMouseEntered
    private void areaFObjMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_areaFObjMouseExited
    }//GEN-LAST:event_areaFObjMouseExited

private void btnNewWindowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewWindowActionPerformed
// TODO add your handling code here:
    
    Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();

    InteraccionesView inte = new InteraccionesView();
//    int height = pantalla.height;
//    int width = pantalla.width;
//    inte.setSize(width/2, height/2);
    inte.setTitle("Interacciones de la Función Objetivo");
    inte.setLocationRelativeTo(null);
    inte.setVisible(true);
    
    inte.presentar(intera);
    
}//GEN-LAST:event_btnNewWindowActionPerformed

private void btnNewFunctionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNewFunctionActionPerformed
// TODO add your handling code here:
    
    activarCampos();
    limpiarContenido();
    funcionEvaluar.setText("");
    funcionIteracciones.setText("");
    
}//GEN-LAST:event_btnNewFunctionActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel areaFObj;
    private javax.swing.JPanel areaRest;
    private javax.swing.JButton btnNewFunction;
    private javax.swing.JButton btnNewWindow;
    private java.awt.Button btnagregarRestriccion;
    private java.awt.Button btnlimpiar;
    private java.awt.Button btnresolver;
    private javax.swing.JComboBox cmbmaxmin;
    private javax.swing.JComboBox cmbsimbolo;
    private javax.swing.JTextArea funcionEvaluar;
    private javax.swing.JTextArea funcionIteracciones;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private java.awt.List listaResultado;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JTable tablaRestricciones;
    private java.awt.TextField txtfobjetivo;
    private java.awt.TextField txtnsubindices;
    private java.awt.TextField txtnvalor;
    // End of variables declaration//GEN-END:variables
    private final Timer messageTimer;
    private final Timer busyIconTimer;
    private final Icon idleIcon;
    private final Icon[] busyIcons = new Icon[15];
    private int busyIconIndex = 0;
    private JDialog aboutBox;
}
