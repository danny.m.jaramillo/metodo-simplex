
package simplex.formulas;


public class ClaseSimplex {

    private static double terminosAuxiliar[] = new double[15];
    private static double terminos[];
    static int indMax = 0;  
    
    public static double[] obtenerValores(double [] entrada){
        double[] resultado=new double[entrada.length];
        for(int i=0;i<entrada.length;i++){
            resultado[i]=entrada[i];
        }
        return resultado;
    }

    public static double[] recortarArreglo(double[] entrada, int indice) {
        double ayuda[] = new double[indice];
        for (int i = 0; i < indice; i++) {
            ayuda[i] = entrada[i];
        }
        return ayuda;
    }

    public static double[][] recortarMatriz(double[][] entrada, int indicex, int indicey) {
        double ayuda[][] = new double[indicex][indicey];
        for (int i = 0; i < indicex; i++) {
            for (int ii=0;ii<indicey;ii++){
            ayuda[i][ii] = entrada[i][ii];
            }
        }
        return ayuda;
    }


    static int indice = 0;

   //Convierte la cadena de caracteres a un vector de doubles
    //Se tranforma la ecuacion y se devuelven los subindicies
    public static double[] capturar(String entrada) {
        for (int n = 0; n < terminosAuxiliar.length; n++) {
            terminosAuxiliar[n] = 0;
        }
        indMax = 0;
        String termino = "";
        indice = 0;
        
        entrada = entrada + ":"; // Donde termina
        //
        while (entrada.length() - 1 > 0) {

            indice = siguienteSigno(entrada); //2x1+6x2
            termino = entrada.substring(0, indice); // 2x1
            entrada = entrada.substring(indice);//+6x2
            separar(termino);
        }
        terminos = new double[indMax];
        System.arraycopy(terminosAuxiliar, 0, terminos, 0, indMax);
        return terminos;
    }

    
    /// Encuntra la posicion donde se encuentra el proximo signo
    private static int siguienteSigno(String entrada) {
        int nn = 0;
        for (int i = 1; i <= entrada.length() - 1; i++) {
            if (entrada.charAt(i) == '+' || entrada.charAt(i) == '-' || entrada.charAt(i) == ':') {
                nn = i;
                i = entrada.length();
            }
        }
        return nn;
    }
    private static void separar(String termino) {
        int ind = 0;
        try {
            if (termino.charAt(0) == '+') {
                termino = termino.substring(1);
            }
            if (termino.charAt(0) == 'x') {
                termino = "1" + termino;
            }
            if (termino.charAt(0) == '-' && termino.charAt(1) == 'x') {
                termino = "-1" + termino.substring(1);
            }
        } catch (Exception ex) {
            System.out.println("ERROR DE TERMINO en separar()");
        }

       
        if (termino.contains("x")) {
            ind = Integer.parseInt(termino.substring(termino.indexOf('x') + 1));
            terminosAuxiliar[ind - 1] = Double.parseDouble(termino.substring(0, termino.indexOf('x')));
        }
        if (ind > indMax) {
            indMax = ind;
        }
    }
}
